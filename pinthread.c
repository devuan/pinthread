/*
 * Copyright (c) 2015 Franco (nextime) Lanza <nextime@nexlab.it>
 *
 * pinthread [https://git.devuan.org/packages-base/pinthread]
 *
 * pinthread is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************************
 *
 * compile with :
 * $ gcc -Wall -D_GNU_SOURCE -fpic -shared -o pinthread.so pinthread.c -ldl -lpthread
 *
 * and test it with :
 * LD_PRELOAD=/path/to/pinthread.so  something
 * 
 * Environment variables:
 *   - PINTHREAD_CORE: set to the core number you want to pin to.
 *                     if omitted, it defaults to the result of 
 *                     sched_getcpu()
 *
 *   - PINTHREAD_PNAMES: accept a space separated list of strings,
 *                       if any string match the process name or
 *                       PINTHREAD_PNAMES is empty/omitted, it will 
 *                       pin all thread according to PINTHREAD_CORE, 
 *                       otherwise it will just call the "real" 
 *                       pthread_create without any pinning.
 *
 *   - PINTHREAD_DEBUG: if set, enable debug messages on stderr.
 */

//#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h> 
#include <dlfcn.h>
#include <sched.h>   //cpu_set_t , CPU_SET
#include <libgen.h>
#include <stdbool.h>
#include <string.h>
#include <stdarg.h>

static char *procname;
static bool pinthread_override = false;
static unsigned int ncore;
static unsigned int setcore;
static bool pinthread_debug = false;

static cpu_set_t mask;

static int (*real_pthread_create)(pthread_t  *thread,
                          const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg);


static int (*real_pthread_setaffinity_np)(pthread_t thread, size_t cpusetsize,
                                          const cpu_set_t *cpuset);


static int (*real_pthread_attr_setaffinity_np)(pthread_attr_t *attr,
                                          size_t cpusetsize, const cpu_set_t *cpuset);


static int (*real_sched_setaffinity)(pid_t pid, size_t cpusetsize,
                                  const cpu_set_t *mask);


static void pint_debug(const char *msg, ...)
{
   va_list args;
   char *str;
   int len;

   if(pinthread_debug)
   {
      va_start(args, msg);
      len = vsnprintf(NULL, 0, msg, args);
      if ((str = malloc(len+1)) == NULL)
         return;

      vsnprintf(str, len+1, msg, args);
      va_end(args);

      fprintf(stderr, "D:PINTHREAD: %s\n", str);
      free(str);
   }
}

static void main_pinthread(int argc, char* argv[], char* envp[])
{
   char *pch;
   char *msg;
   CPU_ZERO(&mask);       /* Initialize it all to 0, i.e. no CPUs selected. */

   procname = basename(argv[0]);
    
   msg = getenv("PINTHREAD_DEBUG");
   if(msg != NULL)
      pinthread_debug = true;

   msg = getenv("PINTHREAD_PNAMES"); 
   if(msg == NULL)
   {
      pinthread_override = true;
   } else {
      pch = strtok (msg," ");
      while (pch != NULL)
      {
         if (!strcmp(procname,pch))
         {
            pinthread_override = true;
            pch = NULL;
         } else {
            pch = strtok (NULL," ");
         }
      }
   }  

   pint_debug("pinthread_main loaded");

   ncore = sysconf (_SC_NPROCESSORS_CONF);
   msg = getenv("PINTHREAD_CORE");
   if (msg != NULL)
   {
      setcore = (unsigned int) strtoul(msg, (char **)NULL, 10);
      pint_debug("Setting core as configured: %u", setcore);
      if(setcore >= ncore)
      {
         fprintf(stderr, "E:PINTHREAD wrong value for PINTHREAD_CORE: %u - using default.\n", setcore);
         setcore = sched_getcpu();
      } 
   } else {
      setcore = sched_getcpu();
      pint_debug("Setting core by sched_getcpu: %u", setcore);
   } 
  
   CPU_SET(setcore, &mask);

   
   pint_debug("Set real_sched_setaffinity");
   real_sched_setaffinity = dlsym(RTLD_NEXT,"sched_setaffinity");
   if ((msg=dlerror())!=NULL)
      fprintf(stderr, "E:PINTHREAD sched_setaffinity dlsym failed : %s\n", msg);


   // make sure the main thread is running on the same core:
   real_sched_setaffinity(getpid(), sizeof(mask), &mask);
   pint_debug("Affinity of main process configured");

   pint_debug("Set real_pthread_create");
   real_pthread_create = dlsym(RTLD_NEXT,"pthread_create");
   if ((msg=dlerror())!=NULL)
      fprintf(stderr, "E:PINTHREAD pthread_create dlsym failed : %s\n", msg);

   pint_debug("Set pthread_setaffinity_np");
   real_pthread_setaffinity_np = dlsym(RTLD_NEXT,"pthread_setaffinity_np");
   if ((msg=dlerror())!=NULL)
      fprintf(stderr, "E:PINTHREAD pthread_setaffinity_np dlsym failed : %s\n", msg);   

   pint_debug("Set real_pthread_attr_setaffinity_np");
   real_pthread_attr_setaffinity_np = dlsym(RTLD_NEXT,"pthread_attr_setaffinity_np");
   if ((msg=dlerror())!=NULL)
      fprintf(stderr, "E:PINTHREAD pthread_attr_setaffinity_np dlsym failed : %s\n", msg);

}

__attribute__((section(".init_array"))) void (* p_main_pinthread)(int,char*[],char*[]) = &main_pinthread;


int pthread_create(pthread_t  *thread,
                   const pthread_attr_t *attr,
                   void *(*start_routine) (void *), void *arg)
{
  int ret;

  pint_debug("about to call original pthread_create");

  ret = real_pthread_create(thread,attr,start_routine,arg);
  if(pinthread_override)
  {
     pint_debug("Overriding pthread_create");
     real_pthread_setaffinity_np(*thread, sizeof(mask), &mask);
  }
  return ret;
}


/* 
 * We need to override also pthread_setaffinity family calls to manage cases in which 
 * the running program try to change it by itself
 */

int pthread_setaffinity_np(pthread_t thread, size_t cpusetsize,
                           const cpu_set_t *cpuset)
{
   pint_debug("about to call original pthread_setaffinity_np");
   if(pinthread_override)
   {
      pint_debug("Overriding pthread_setaffinity_np");
      return real_pthread_setaffinity_np(thread, sizeof(mask), &mask);
   } 
   return real_pthread_setaffinity_np(thread, cpusetsize, cpuset);
}

int pthread_attr_setaffinity_np(pthread_attr_t *attr,
                                size_t cpusetsize, const cpu_set_t *cpuset)
{

   pint_debug("about to call original pthread_attr_setaffinity_np");
   if(pinthread_override)
   {
      pint_debug("Overriding pthread_attr_setaffinity_np");
      return real_pthread_attr_setaffinity_np(attr, sizeof(mask), &mask);
   }
   return real_pthread_attr_setaffinity_np(attr, cpusetsize, cpuset);
}


int sched_setaffinity(pid_t pid, size_t cpusetsize,
                      const cpu_set_t *omask)
{
   pint_debug("about to call original sched_setaffinity");
   if(pinthread_override)
   {
      pint_debug("Overriding sched_setaffinity");
      return real_sched_setaffinity(pid, sizeof(mask), &mask);
   }
   return real_sched_setaffinity(pid, cpusetsize, omask);

}

